<?php

# Weather
# GET API key from openweathermap
if (USE_WEATHER && isset($_GET['get_weather'])) {

    $url = "https://api.openweathermap.org/data/2.5/weather?lat={$_GET['lat']}&lon={$_GET['lon']}&appid=".WEATHER_API_KEY."&units=metric";

    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, '');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out,true);
        echo json_encode(array(
            "temp"=>$object["main"]["temp"],
            "wind"=>$object["wind"]["speed"],
            "clouds"=>$object["clouds"]["all"]
            )
        );
    }

    exit;
}

?>
