<div class="tab-wrapper" style="width:100%" x-data="{ 
        activeTab: 0,
        speciesCount: {},
    }">
      <div class="scroll-container">
      <div class="tab-navi scroll-content">
        <div class="scroll-item active"
          @click="activeTab = 0"
          class="tab-control"
          :class="{ 'active': activeTab === 0 }"
          x-data="{}"
        >Ravaszdi Radar</div>
        <div class="scroll-item"
          @click="activeTab = 1"
          class="tab-control"
          :class="{ 'active': activeTab === 1 }"
        >Lokalizálás</div> 
        <div class="scroll-item"
          @click="activeTab = 2"
          class="tab-control"
          :class="{ 'active': activeTab === 2 }"
        >Példányjellemzők</div>
        <div class="scroll-item"
          @click="activeTab = 3"
          class="tab-control"
          :class="{ 'active': activeTab === 3 }"
        >Háttérinformációk</div>
      </div>  
      <div class="scroll-arrows">
        <div class="arrow left"><i class="fa fa-chevron-left" style="color:#fff;padding-top:.5rem"></i></div>
        <div class="arrow right"><i class="fa fa-chevron-right" style="color:#fff;padding-top:.5rem"></i></div>
    </div>
</div>