<link rel="stylesheet" href="../../styles/ravaszdi_choose.css">

<div class="tab-panel" :class="{ 'active': activeTab === 0 }" x-show.transition.in.opacity.duration.600="activeTab === 0">
      <div style="padding:0.5rem 0 0.5rem 0;line-height:1.3;letter-spacing:0.4px;text-align:justify;border-bottom:3px solid white;max-width:30rem">Két rendkívül jól alkalmazkodó (és ravasz) ragadozó. Közülük a vörös róka mára nyilvánvalóan beköltözött a lakott területekre, akár nagyvárosokba, és az aranysakál városi megjelenésére is vannak már példák A Ravaszdi Radar önkéntes adatgyűjtő program elsődleges célja e két faj városiasodásának vizsgálata és nyomon követése, közösségi segítséggel: bárki, aki rókát vagy sakált lát, beküldheti az észlelését és annak körülményeit, ezzel nagyban segítve a szakemberek munkáját. Elsődlegesen a lakott területeken történt észlelések lényegesek, de minden, a két faj hazai előfordulására vonatkozó megfigyelés hasznos lehet és örömmel fogadjuk.</div>
        <div style="padding:0.5rem 0 0.5rem 0;text-align:center;color:rgb(95,96,114);font-style:italic;font-weight:bold;border-bottom:3px solid white;max-width:30rem">Észlelés esetén - vadállatokról lévén szó - tartson tőlük biztonságos távolságot!</div>
        <div  x-show="!cookieExists"></div>
        <br>   
        <div><a href='https://openbiomaps.org/projects/ravaszdi/?login'>Bejelentkezés*</a>
            <form id="insects-form" action="" class="pure-form panel-form">
                <div style="padding:0.5rem;text-align:center"></div>
                <div class="label"; style="font-weight:bold;letter-spacing:1.4px">Mit láttál?* 
                            </a></div>
                <br>
                <div class = "image-picker">
                    <label>
                        <i title="VÖRÖS RÓKA testhossza 60-90 cm, testtömege 3-10 kg, bundája vöröses, a háton sötétebb, a has szőrzete szürkésfehér vagy fehéres. Farka hosszabb (35-40 cm), mint a sakálé, a vége rendszerint fehér. A lábak külső fele és a fülek külső felülete fekete.">
                            <input type="radio" name="species" value="fox" checked>
                            <img src="images/roka.png" alt="Fox">
                        </i>
                    </label>
    
                    <label>
                        <i title ="Az ARANYSAKÁL testhossza 80-100 cm, testtömege 7-15 kg, bundája vöröses és sárgásbarna, hátán a szőr szürkés, feketés, hasoldala világos. Farka a rókáénál rövidebb (20-25 cm) és a végén a legsötétebb, feketésbarna színű. A fülek külső felülete vörösessárga.">
                            <input type="radio" name="species" value="jackal" checked>
                            <img src="images/sakal.png" alt="Jackal">
                        </i>
                    </label>
                </div>
            </form>
        </div>
      </div>