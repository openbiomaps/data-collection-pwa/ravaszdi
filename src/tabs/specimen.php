<div class="tab-panel" :class="{ 'active': activeTab === 2 }" x-show.transition.in.opacity.duration.600="activeTab === 2">
        <div x-data="{}">
            <form id="plants-form" action="" class="pure-form panel-form">
                <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white"></div>

                <div class="label">Hány egyedet észlelt?*</div>
                <input type="number" name="vegetationHeight" id="vegetationHeight" min="1" max="20" class="pure-u-1" required>
                
                <div class="label">Milyen volt az észlelt egyed(ek) életkora?*</div>
                <select id="plantCover" name="plantCover" class="pure-u-1" required>
                    <option value="" disabled selected>Válasszonon az alábbi lehetőségek közül!</option>
                    <option value="juvenile">kölyök (adott évben született)</option>
                    <option value="adult">felnőtt</option>
                    <option value="unknown">nem tudom megállapítani</option>
                </select>
 
                 <div class="label">Milyen volt a megfigyelt egyed(ek) állapota?*</div>
                 <select id="flowerSpecies" name="flowerSpecies" class="pure-u-1" required>
                    <option value="" disabled selected>Válasszon az alábbi lehetőségek közül!</option>
                    <option value="healthy">élő példány, látszólag egészséges</option>
                    <option value="injured">élő példány, szemmel látható külső sérüléssel</option>
                    <option value="sick">élő példány, szemmel láthatóan beteg</option>
                    <option value="trapped">élő példány, élvefogó csapdában</option>
                    <option value="dead">elpusztult állat teteme</option>
                </select>

                <div class="label">Mi volt a sérülés/pusztulás/betegség oka?*</div>
                <select id="flowerNumber" name="flowerNumber" class="pure-u-1" required>
                    <option value="" disabled selected>Válasszon az alábbi lehetőségek közül!</option>
                    <option value="roadkilled">gázolás miatt elpusztult/sérült</option>
                    <option value="stucked">kerítésbe szorult</option>
                    <option value="trappkiled">ölőcsapdában talált</option>
                    <option value="hunted">vadászat során elejtett</option>
                    <option value="scabies">rühes (kiterjedt szőrhullás, bőr megkeményedése)</option>
                    <option value="neurologic">idegrendszeri tüneteket mutat (ferde fejtartás, remegés, görcsök, bénulás)</option>
                    <option value="weakened">kórosan sovány, legyengült</option>
                    <option value="other">egyéb:</option>
                </select>

                <div class="label">Okoz-e konfliktust az észlelt egyed (vagy egyedek) az emberek szempontjából?</div>
                <select id="flowerConflict" name="flowerConflict" class="pure-u-1">
                    <option value="" disabled selected>Válasszon az alábbi lehetőségek közül!</option>
                    <option value="poultry">igen, baromfi, egyéb haszonállatok zsákmányolásával</option>
                    <option value="alarmism">igen, jelenléte a helyben élőkben riadalmat kelt</option>
                    <option value="pets">igen, a háziállatokat (kutyát, macskát) támadja</option>
                    <option value="otherkind">igen, egyéb módon</option>
                    <option value="no">nem</option>
                    <option value="unknown">nem tudom</option>
                    <option value="otherconflikt">egyéb konfliktusok:</option>
                </select>

            </form>
        </div>
      </div>