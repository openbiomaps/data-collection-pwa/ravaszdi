<div class="tab-panel" :class="{ 'active': activeTab === 3 }" x-show.transition.in.opacity.duration.600="activeTab === 3">
    <!--     <form id='enviroment-form' action="/action_page.php" class="pure-form panel-form">

            <div style="padding:0.5rem 0 0.5rem 0;text-align:center;border-bottom:1px solid white;max-width:30rem">Környezeti állapotokra vonatkozó adatok. Amennyiben van internet kapcsolatod és GPS hozzáférésed, akkor automatikusan kitöltésre kerül.<br>Ha nincsenek automatikusan beírt értékek, vagy nem tűnnek megfelelőnek, add meg őket.</div>
            <div x-data="{
                    weather: '',
                    async getPosition() {
                        let ll = await getPosition();
                        this.weather = await retrieveWeather(ll.lon,ll.lat);
                    },
                    getCurrentDateTime() {
                        const now = new Date();
                        return formatDatetime(now);
                    }
                }"
                x-init="getPosition">

          <div class="label">Szél</div>
            <select class="pure-u-1 dropdown-menu-js plant-options" id="wind" name="wind" x-model="weather.wind">
                <option value="" disabled selected>Adja meg a szél erősséget...</option>
                <template x-for="option in windOptions" :key="option">
                    <option :value="option" x-text="option" :selected="convertWindCategory(weather.wind) === option"></option>
                </template>
            </select>

            <div class="label">Hőmérséklet</div>
            <input type="text" name="temperature" id="temperature" class="pure-u-1" placeholder="Adja meg a hőmérsékletet" x-model="weather.temp" >

            <div class="label">Árnyék</div>
            <select class="pure-u-1 dropdown-menu-js plant-options" id="shadow" name="shadow" x-model="weather.clouds">
                <option value="" disabled selected>Adja meg az árnyékoltság mértékét...</option>
                <template x-for="option in shadowOptions" :key="option">
                    <option :value="option" x-text="option" :selected="convertShadowCategory(weather.clouds) === option"></option>
                </template>
            </select>

            <div class="label">Megfigyelés időpontja</div>
            <input type="datetime-local" name="date" id="date" x-model="getCurrentDateTime()" style="width:100%" min="2024-01-01T00:01"> 
            </div>
        </form> -->

    <div class="label">Ismeretei szerint mikor jelent meg az adott faj ezen a területen?</div>
    <br>
    <select id="FlowerAppearance" name="FlowerAppearance" class="pure-u-1 dropdown-menu-js">
        <option value="" disabled selected>Kérjük adja meg az időpontot, ha ismert!</option>
        <option value="appearance">Tudtommal az első megfigyelés becsült időpontja:</option>
        <option value="unknown">Nincs róla ismeretem</option>
    </select>
    <div> <br> </div>
    <div class="label">Melyik állítás igaz Önre az alábbiak közül? (Az adatok értékeléséhez szükséges információ.)</div>
    <br>
    <select id="plantCover" name="plantCover" class="pure-u-1">
        <option value="" disabled selected>Kérjük válasszon az alábbi lehetőségek közül!</option>
        <option value="local">Helyi lakos vagyok</option>
        <option value="many">Nem vagyok helyi lakos, de többször jártam itt</option>
        <option value="once">□ Nem vagyok helyi lakos, csak egyszer jártam itt </option>
    </select>
    <div> <br> </div>
    <div class="label">Egyéb megfigyelés:</div>
    <br>
    <textarea type="textarea" name="Remark" id="Remark" class="pure-u-1" placeholder="Mit csinált az állat, szelídnek látszott-e stb.), megjegyzés (bármi más, ami fontos vagy érdekes lehet)"></textarea>
    <br>

    <!--<button x-on:click.prevent="submit(speciesCount)" class="pure-button button-success form-submit"> Adatok beküldése </button>-->
    <button x-on:click.prevent="form_check()" class="pure-button button-success form-submit">
        <span id="buttonText">Adatok beküldése</span>
        <span id="loadingIcon" class="material-icons" style="display: none;">autorenew</span> </button>
</div>
</div>