<div class="tab-panel" :class="{ 'active': activeTab === 1 }" x-show.transition.in.opacity.duration.600="activeTab === 1">
    <div x-data="newplotComponent()" x-init="getPosition()">
        <form id="samplinglocation-form" action="" class="pure-form panel-form">

            <div x-show="selected_sample_plot" x-data x-init="$watch('$store.x.sites', () => $store.x.selectedSite = Object.keys($store.x.sites)[0])">
                <div @click="openForm = !openForm" style="padding:0.5rem;text-align:center;border-bottom:1px solid white">Észlelés helyének meghatározása* <!-- <i class="material-icons" title="Térkép/Űrlap" style="font-size:24px">map</i> -->
                    <br>
                </div>
                <!-- <div x-show="!cookieExists">
                        <a href='https://openbiomaps.org/projects/ravaszdi/?login'>Bejelentkezés</a>
                    </div>
                    <a href='' @click.prevent="new_sample_plot = !new_sample_plot;selected_sample_plot = !selected_sample_plot">Új mintavételi hely felvétele</a>
    
                    <div class="label">Mintavételi hely</div>-->
                <!-- <select id='site_name' class="" name="site_name" 
                        x-model="$store.x.selectedSite" >
                        x-on:change="$store.x.selectedSite == 'k0' ? setTab(0) : setTab(1)">
                      <template x-for="(value, key) in $store.x.sites">
                         <option :value="key" x-text="$store.x.sites[key].name"></option>
                      </template>
                    </select>
                    <div class="label">Megfigyelő</div>
                    <input id='observer' name="observer" placeholder="megfigyelő neve" 
                        x-bind:value="$store.x.sites[$store.x.selectedSite].location_type !== 'magán' ? '' : $store.x.sites[$store.x.selectedSite].observer"                
                     > 
                </div>

                <div id='new_sample_plot_div' x-show="new_sample_plot">
                    <div style="padding:0.5rem;text-align:center;border-bottom:1px solid white">Új mintavételi hely felvétele. 
                        <a href='https://pollinator-monitoring.hu/protocoll' target="_blank"><i class="material-icons" style="font-size:24px;vertical-align:bottom">help</i></a></div>

                  <br> -->
                <!-- <div x-show="!cookieExists">
                        <a href='https://openbiomaps.org/projects/pollimon/?login'>Bejelentkezés</a>
                    </div> -->

                <!-- <a href='' @click.prevent="new_sample_plot = !new_sample_plot;selected_sample_plot = !selected_sample_plot">Új mintavétel kiválasztott helyen</a> -->

                <!-- <div class="label">Nevezd el mintavételi helyszínt</div>
                    <input class="pure-u-1" type="text" placeholder="Adj egy nevet a helynek" name="newplotName" id="newplotName" x-model="streetName">
                    
                    <input id='new_observer' name="new_observer" placeholder="megfigyelő neve" x-model="observer">-->

                <div class="label">Észlelés helyének jellege*</div>
                <select id="habitatType" name="habitatType" class="pure-u-1" required>
                    <option value disabled selected>Válasszon egy jellemzőt!</option>
                    <!-- template x-for="(name, type) in habitatList" :key="type">
                        <option :value="type" x-text="name"></option> -->
                    </template>
                    <option value="center">településközpont, sűrűn beépített városias lakóövezet</option>
                    <option value="industrial">egyéb sűrűn beépített terület (ipari övezet, kereskedelmi övezet stb.)</option>
                    <option value="suburban">kertes lakóövezet faluban, városban</option>
                    <option value="mixed">vegyes művelésű, szórtan beépített zártkert</option>
                    <option value="park">közpark, temető</option>
                    <option value="arable">szántóföld, parlag</option>
                    <option value="orchard">gyümölcsös, szőlőültetvény</option>
                    <option value="grass">gyep (rét, legelő, kaszáló)</option>
                    <option value="bushy">bozótos, cserjés</option>
                    <option value="forest">erdő, fás terület</option>
                    <option value="wetland">vízpart, nádas, mocsár</option>
                </select>

                <div class="label">Megfigyelés időpontja*</div>
                <input type="datetime-local" name="observation-date" id="observation-date" style="width:100%" required>
                <div class="label">Készíts egy fényképet az állatról! Töltsön fel fotókat az észlelés kapcsán, a megfigyelt állat(ok)ról, az élőhelyről! Lehetőség szerint a felvételeken alapján a teljes állat kivehető legyen.</div>
                <nobr><i class="material-icons" style="font-size:24px">camera</i>&nbsp;<input type="file" id="photoInput" name="photoInput" accept="image/x-png,image/jpeg,image/gif" class="pure-u-1" /></nobr>
                <img id="capturedPhoto" style="max-height:10rem" />

                <div class="label">GPS pozíció*</div>
                <!-- <input id="gps_pos" name="gps_pos" x-model="gps_pos"> -->
                <div x-data="geoComponent('gps_pos')" x-init="init" x-on:unload="destroy">
                    <input id="gps_pos" type="text" readonly="">
                </div>
            </div>
        </form>
    </div>
</div>