
<div class="form-container resizer" id="form-container" x-show="openForm">

    <!-- Navigator -->
    <?php include 'navigator.php'; ?>

      <!-- First tab -->
      <!-- Ravaszdi választás -->
      <?php include 'tabs/ravaszdi_choose.php'; ?>
      
      <!-- Second tab -->
      <!-- Lokalizálás -->
      <?php include 'tabs/localisation.php'; ?>
      
      
      <!-- Third tab -->
      <!-- Példányjellemzők -->
      <?php include 'tabs/specimen.php'; ?>
      
      
      <!-- Forth tab -->
      <!-- Háttérinformációk -->
      <?php include 'tabs/background_info.php'; ?>
      
</div> <!-- Form end -->