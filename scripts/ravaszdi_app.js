const speciesList = {
    'roka': 'Róka',
    'sakal': 'Aranysakál',
}


const habitatList = {
    'center': 'településközpont, sűrűn beépített városias lakóövezet',
    'industrial': 'egyéb sűrűn beépített terület (ipari övezet, kereskedelmi övezet stb.)',
    'suburban': 'kertes lakóövezet faluban, városban',
    'mixed': 'vegyes művelésű, szórtan beépített zártkert',
    'park': 'közpark, temető',
    'arable': 'szántóföld, parlag',
    'orchard': 'gyümölcsös, szőlőültetvény',
    'grass': 'gyep (rét, legelő, kaszáló)',
    'bushy': 'bozótos, cserjés',
    'forest': 'erdő, fás terület',
    'wetland': 'vízpart, nádas, mocsár'
}
const windOptions = [
    'nincs','gyenge szellő','enyhe szél','gyenge szél',
    'mérsékelt szél','erős szél'
];
const shadowOptions = ['nincs','részleges','teljes'];

function updateSpeciesCount(speciesCount, species, count) {
    if (speciesCount[species]) {
        speciesCount[species].count += count;
        //if (count) {
        //    speciesCount[species].position.push('1x1');
        //} else {
        //    speciesCount[species].position.splice(-1);
        //}
    } else {
        speciesCount[species] = {
            count:count,
        //    position:['1x1']
        };
    }
}
function addNewSpecies(newSpeciesInput, speciesCount, species, event, speciesList) {
    const newSpeciesName = event.target.value;
    if (newSpeciesName) {
        if (species === 'user_input') {
            speciesList[newSpeciesName] = newSpeciesName;
            speciesCount[newSpeciesName] = {
                count: 0,
            //    position: []
            }
            updateSpeciesCount(speciesCount, newSpeciesName, 1);
            delete speciesCount[species];
            newSpeciesInput[newSpeciesName] = newSpeciesName + ' / 1';
        }
    }
}
function incrementCount(speciesCount, newSpeciesInput, species, speciesList) {
    updateSpeciesCount(speciesCount, species, 1);
    if (newSpeciesInput[species]) {
        newSpeciesInput[species] = speciesList[species] + ' / ' + speciesCount[species].count;

    }
}

function decreaseCount(speciesCount, newSpeciesInput, species,speciesList) {
    if (speciesCount[species].count > 0) {
        updateSpeciesCount(speciesCount, species, -1);
        if (speciesCount[species].count === 0) {
            delete speciesCount[species];
        }
        else if (newSpeciesInput[species]) {
            newSpeciesInput[species] = speciesList[species] + ' / ' + speciesCount[species].count;
        }
    }
}
// Új mintavételi hely
function newplotComponent() {
    return {
      selectedHabitat: '',
      selectedLine: '',
      habitatList: habitatList,
      streetName: '',
      observer: '',
      gps_pos: '',
      cookieExists: document.cookie.indexOf('access_token') >= 0,
      async getPosition() {
            ll = await getPosition();
            this.streetName = await getStreetName(ll.lon,ll.lat);
            this.gps_pos = ll.lon + "," +ll.lat;
      },
      async getObserver() {
            ll = await getObserver();
            this.observer = await getObserver();
      }
    };
}

// Weather data transformation
function convertWindCategory(wind) {
        if (wind <= 2) {
            return 'nincs';
        } else if (wind <= 6) {
            return 'gyenge szellő';
        } else if (wind <= 11) {
            return 'enyhe szél';
        } else if (wind <= 19) {
            return 'gyenge szél';
        } else if (wind <= 29) {
            return 'mérsékelt szél';
        } else if (wind >= 30 ) {
            return 'erős szél';
        }
}
function convertShadowCategory(cloud) {
        if (cloud <= 10) {
            return 'nincs';
        } else if (cloud <= 85) {
            return 'részleges';
        } else if (cloud <= 100) {
            return 'teljes';
        } else {
            return '';
        }
}


// Form submit
function submit(speciesCount,plantCount) {
    let plants = plantCount;
    let insects = speciesCount;
    let post = [];

    let date = $("#date").val(); // Environment tab 

    // Site tab
    let site_name = $("#site_name").val();
    let observer = $("#observer").val();
    let management = ""; //$("#management").val();
    let gps_pos = $("#gps_pos").val();

    // New site tab
    if ($("#newplotName").val() != "") {
        site_name = $("#newplotName").val();
        observer = $("#new_observer").val();
        let habitatType = $("#habitatType").val();  // Élőhelytípus: lágyszárú, bokros, fás
        let fileInput = $("#photoInput")[0];
        var photo = fileInput.files[0]; 
        let locationType = $("#plotLocationType").val(); // közterület, magán
        management = ""; //$("#new_management").val();

        // New site form
        post = [];
        let record = {
            "date": date,
            "observer" : observer,
            "observer_email" : "",
            "name" : site_name,
            "location_type": locationType,
            "environment": habitatType,
            //"management": management,
            "obm_files_id": photo,
            "obm_geometry": gps_pos,
        }
        post.push(record);
        console.log(post);
        form_send(1038,post);
    }



    // Observations
    // Environment tab
    let wind = $("#wind").val();
    let temperature = $("#temperature").val();
    let shadow = $("#shadow").val();

    // Obsevation id
    let observation_id = generateUniqueHash(date + site_name + observer);

    // Pollinator form data
    let pollinators = {};
    let n = 0;
    for (let key in insects) {
        if (insects.hasOwnProperty(key)) {
            let insect = insects[key];
            let count = insect.count;
            let species = key;
            //let positions = insect.position;
            for (let i=0;i<count;i++) {
                pollinators[n] = {
                    "species_category" : species, // predefined categories
                    "species" : "", // other
                    "count" : 1,
                    //"position" : positions[i] || '1x1',
                }
                n++;
            }
        }
    }

    post = [];
    let keys = Object.keys(pollinators);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        let p = pollinators[key];

        let record = {
            "date": date,
            "observer" : observer,
            "observer_email" : "",
            "observation_id": observation_id,
            "location_name" : site_name,
            "shadow": shadow,
            "wind": wind,
            "temperature": temperature,
            "species" : p.species,
            "species_category": p.species_category,
            "noindividuals": p.count, // 1
            //"obm_geometry": "",
        }
        post.push(record);
    }
    console.log(post);
    form_send(1039,post);

    post = [];

    record = {
        "date": date,
        "observer": observer,
        "observer_email" : "",
        "observation_id": observation_id,
        "location_name": site_name,
        "vegetation_height": $("#vegetationHeight").val(),
        "plant_cover": $("#plantCover").val(),
        "flower_species": $("#flowerSpecies").val(),
        "plant_num": $("#flowerNumber").val(),
        //"obm_geometry": "",
        //"obm_files_id": "",
        //"field_state": fstate,
    }
    post.push(record);
    console.log(post);
    form_send(1040,post);

    speciesCount = {};
    plantCount = {};
}

async function form_send(form_id,post) {

    const access_token = await refreshToken();
    const headers = [];
    for (const key in post[0].data) {
        if (post[0].hasOwnProperty(key)) {
            if (typeof post[0][key] !== 'undefined') {
                headers.push(key);
            }
        }
    }

    const metadata = [];
    const formData = new FormData();
    formData.append('access_token', access_token.access_token);
    formData.append('form_id', form_id);
    formData.append('scope', 'put_data');
    formData.append('header', JSON.stringify(headers));
    formData.append('batch', JSON.stringify(post));
    formData.append('metadata', JSON.stringify(metadata));
    for(let i = 0; i <images.length; i++) {
        console.log(images[i]);
        formData.append(`file${i}`, images[i]);

    }
    
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "https://"+ URL +"/v"+ API_VERSION +"/pds.php");
    
    xhr.onload = () => {
      if (xhr.readyState == 4 && xhr.status == 201) {
        console.log(JSON.parse(xhr.responseText));
      } else {
        console.log(`Error: ${xhr.status}`);
      }
    };
    xhr.send(formData);

}
