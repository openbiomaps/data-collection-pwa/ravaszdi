    /*var wmsLayer = new ol.layer.Image({
        //extent: [-13884991, 2870341, -7455066, 6338219],
        source: wmsSource
    });*/

    var currentPosition;

    const map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),
          //wmsLayer,
          clusters,
          drawLayer,
          markerLayer,
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat([18.854118,47.458825]),
          projection: projection,
          zoom: 8
        })
    });

    /* GeoLocation info:
        - Speed info
        - Accuracy info
        - Position symbol
        - Accuracy symbol
     */
    const geolocation = new ol.Geolocation({
      // enableHighAccuracy must be set to true to have the heading value.
      trackingOptions: {
        enableHighAccuracy: true,
      },
      projection: map.getView().getProjection(),
    });
    geolocation.setTracking(true);
    function el(id) {
      return document.getElementById(id);
    }
    /*el('track').addEventListener('change', function () {
      geolocation.setTracking(this.checked);
    });*/
    geolocation.on('change', function () {
      el('accuracy').innerText = Math.round(geolocation.getAccuracy() * 10) / 10 + ' [m]';
    //  el('altitude').innerText = geolocation.getAltitude() + ' [m]';
    //  el('altitudeAccuracy').innerText = geolocation.getAltitudeAccuracy() + ' [m]';
    //  el('heading').innerText = geolocation.getHeading() + ' [rad]';
      el('speed').innerText = Math.round(geolocation.getSpeed() * 36) / 10 + ' [km/h]';
      el('gps_pos').innerText = geolocation.getPosition()[0] + "," + geolocation.getPosition()[1];
    });
    // handle geolocation error.
    geolocation.on('error', function (error) {
      const info = document.getElementById('geoinfo');
      info.innerHTML = error.message;
      info.style.display = '';
    });
    const accuracyFeature = new ol.Feature();
    accuracyFeature.setStyle(
        new ol.style.Style({
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new ol.style.Stroke({
              color: '#a3a3a3',
              width: 2,
            })
        })
    );
    geolocation.on('change:accuracyGeometry', function () {
      accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
    });
    const positionFeature = new ol.Feature();
    positionFeature.setStyle(
      new ol.style.Style({
        image: new ol.style.Circle({
          radius: 4,
          fill: new ol.style.Fill({
            color: '#ffe81f',
          }),
          stroke: new ol.style.Stroke({
            color: '#3a3a3a',
            width: 2,
          }),
        }),
      })
    );
    geolocation.on('change:position', function () {
        const coordinates = geolocation.getPosition();
        currentPosition = coordinates;
        positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
    });
    var geolocationLayer = new ol.layer.Vector({
      map: map,
      source: new ol.source.Vector({
        features: [accuracyFeature, positionFeature],
      }),
    });


    const modify = new ol.interaction.Modify({source: drawSource});
    map.addInteraction(modify);
         
    /*const snap = new ol.interaction.Snap({
      source: drawLayer.getSource(),
    });
    map.addInteraction(snap);
    */


    let properties;

    map.on('click', (e) => {
        const v = e.coordinate;

        markerSource.clear(true);
        
        // turn off tracking
        navigator.geolocation.clearWatch(watchID);
        document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route" style="color:lightgray"></i>';
        document.getElementById("trackloc").title = "Turn on location tracking";

        // transform click coordinates
        var wgs_v = ol.proj.transform(v, 'EPSG:3857', 'EPSG:4326');

        // streetName
        let streetNamePromise = getStreetName(wgs_v[0],wgs_v[1])
        streetNamePromise.then(function(streetName){
            document.getElementById('newplotName').value = streetName;
        });

        // weather
        let weatherPromise = retrieveWeather(wgs_v[0],wgs_v[1]);
        weatherPromise.then(function(weather) {
            
            document.getElementById('wind').value = convertWindCategory(weather.wind);
            document.getElementById('shadow').value = convertShadowCategory(weather.clouds);
            document.getElementById('temperature').value = weather.temp;
        });

        fixed_position = v;

        const marker = new ol.Feature({
            geometry: new ol.geom.Point(v),
        });
        marker.setStyle(new ol.style.Style({
            image: new ol.style.Icon({
                anchor: [0.5, 1],
                src: 'images/marker.png', 
                size: [20,32],
            }),
        }));
        markerLayer.getSource().addFeature(marker);


        var actualZoom = map.getView().getZoom();
        let zoom;
        if (actualZoom < 18) {
            zoom = eval(actualZoom + 2)
        } else {
            zoom = actualZoom;
        }
        map.getView().animate({center: [v[0],v[1]]}, {zoom: zoom});
    });

    /*
     *   // map.on('click').. folytatása
         const clickedFeatures = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
            if (layer !== null) {
                return feature;
            } else
                return false;
        });
        const info = document.getElementById('info-content');
        Alpine.store('showInfo',true);
        if (clickedFeatures) {
            const cfeatures = clickedFeatures.get('features');
            if (cfeatures.length) {
                let text;
                let n = 0;
                cfeatures.forEach((e, index, arr) => {
                    n++;
                    properties = e.getProperties();
                    Object.keys(properties).forEach(key => {
                        if (key == 'geometry') { return; }
                        info.innerHTML += '<b>' + key + '</b>: ' + properties[key] + '<br>';
                    });
                    info.innerHTML += '<hr>';
                    if (n > 5) {
                        info.innerHTML += '....';
                        arr.length = index + 1;
                    }
                });
            }
        } else {
            //Zoom to click
            let v = e.coordinate;
    */
            
            /*let extent = new ol.extent.boundingExtent([[eval(v[0]-500),eval(v[1]-500)],[eval(v[0]+500),eval(v[1]+500)]]);
            map.getView().fit(extent, {duration: 1000, padding: [50, 50, 50, 50]});*/
    /*
            var actualZoom = map.getView().getZoom();
            map.getView().setCenter([v[0],v[1]]);
            map.getView().animate({center: [v[0],v[1]]}, {zoom: eval(actualZoom + 2)});
        }
    });
    */

    // Hosszú kattintás esemény kezelése
    /*map.on('pointerdown', (event) => {
      // Megjegyezzük a kattintás kezdeti időpontját
      touchStartTimestamp = new Date().getTime();

      // Beállítunk egy időzítőt, amely a hosszú kattintás esetén hívódik meg
      longPressTimeout = setTimeout(() => {
        // Ellenőrizzük, hogy az érintés még mindig folyamatban van-e
        if (new Date().getTime() - touchStartTimestamp >= 500) {
          // Itt írd meg a hosszú kattintás esemény kezelőjét
          console.log('Hosszú kattintás történt!');
        }
      }, 500); // Az időzítő időkorlátja, itt is 500 milliszekunda, de ezt a számot az igényeidhez igazíthatod
    });

    map.on('pointerup', (event) => {
      // Megszakítjuk az időzítőt, ha az érintés véget ér
      clearTimeout(longPressTimeout);
    });*/

    map.on('moveend', function(e) {
        filter();
    });
    
    /* Tracklogging
        - Trackline draw
        - Following geolocation position changes on map
     */
    const tracklineSource = new ol.source.Vector();
    const tracklineLayer = new ol.layer.Vector({
        source: tracklineSource,
        style: new ol.style.Style({
          stroke: new ol.style.Stroke({
              color: [0,0,0,0.6],
              width: 2,
              lineDash: [4,8],
              lineDashOffset: 6
          }),
        }),
    });
    map.addLayer(tracklineLayer);

    const track = document.createElement('div');
    track.className = 'ol-control ol-unselectable track';
    track.innerHTML = '<button id="trackloc" title="Turn off tracking"><i class="fa-solid fa-route" style=""></i></button>';
    map.addControl(new ol.control.Control({
        element: track
    }));

    var watchID;
    var trackWatch = function(e) {
        if (e=='off') {
            navigator.geolocation.clearWatch(watchID);
            document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route" style="color:lightgray"></i>';
            document.getElementById("trackloc").title = "Turn on location tracking";
        } else {
            document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route"></i>';
            document.getElementById("trackloc").title = "Turn off location tracking";
            let trackline_start = 0;
                watchID = navigator.geolocation.watchPosition(function(pos) {
                const coords =  new ol.proj.fromLonLat([pos.coords.longitude, pos.coords.latitude]);

                var start_point = coords;
                var end_point = coords;

                if (!trackline_start) {
                    tracklineSource.addFeatures([
                        new ol.Feature(new ol.geom.LineString([start_point, end_point]))
                    ]);
                    trackline_start = 1;
                } else {
                    let line = tracklineSource.getFeatures()[0].getGeometry();
                    line.appendCoordinate(coords);
                    map.getView().setCenter(coords);
                }

            }, function(error) {
                //alert(`ERROR: ${error.message}`);
                console.log(error.message);
            }, {
                enableHighAccuracy: true
            });
        }
    }
    trackWatch('on');

    var trackLocEnabled = true;
    var toggleLoc = document.querySelector("#trackloc");
    toggleLoc.addEventListener('click', function() {
        if (!trackLocEnabled) {
          trackWatch('on'); // Turn on location.Watch!
          trackLocEnabled = true;
          toggleLoc.title = "Location tracking is enabled";
          toggleLoc.style.color = "lightskyblue";
        } else {
          trackWatch('off'); // Turn off.
          trackLocEnabled = false;
          toggleLoc.title = "Location tracking is disabled";
          toggleLoc.style.color = "darkslategray";
        }
    }, false);

    /* User location */
    const locate = document.createElement('div');
    locate.className = 'ol-control ol-unselectable locate';
    locate.innerHTML = '<button title="Locate me"><i class="fa-solid fa-location-crosshairs"></button>';
    locate.addEventListener('click', function() {
        //if (!accuracySource.isEmpty()) {
        let lon;
        let lat;
        if (!geolocationLayer.getSource().isEmpty()) {
            
            // Itt is kikapcsoljuk a trackelést
            navigator.geolocation.clearWatch(watchID);
            document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route" style="color:lightgray"></i>';
            document.getElementById("trackloc").title = "Turn on location tracking";
            
            // kitöröljük a marker rétegről a marker ikont.
            markerLayer.getSource().clear();
            fixed_position = [];
            // a gps pozícióra lekérdezzük a weather és utca név adatokat
            let llPromise = getPosition();
            llPromise.then(function(ll){
                let weatherPromise = retrieveWeather(ll.lon,ll.lat);
                weatherPromise.then(function(weather) {
                    document.getElementById('wind').value = convertWindCategory(weather.wind);
                    document.getElementById('shadow').value = convertShadowCategory(weather.clouds);
                    document.getElementById('temperature').value = weather.temp;
                });
                let streetNamePromise = getStreetName(ll.lon,ll.lat)
                streetNamePromise.then(function(streetName){
                    document.getElementById('newplotName').value = streetName;
                });
            });

            //map.getView().fit(accuracySource.getExtent(), {
            map.getView().fit(geolocationLayer.getSource().getExtent(), {
                maxZoom: 18,
                duration: 500
            });
        }
    });
    map.addControl(new ol.control.Control({
        element: locate
    }));
    var currZoom = map.getView().getZoom();
    
    // Itt is kikapcsoljuk a trackelést
    map.getView().on('change:resolution', function(event) {    
        navigator.geolocation.clearWatch(watchID);
        document.getElementById("trackloc").innerHTML = '<i class="fa-solid fa-route" style="color:lightgray"></i>';
    });
    
    /*var clearF = function(e) {
        console.log('clear');
        wmsLayer.setVisible(true);
        clusters.setVisible(false);
        clusterSource.getSource().clear(true);
        drawSource.clear(true);
    }*/

    var filter = function(e) {

        var actualZoom = map.getView().getZoom();

        // Prevent fetching too much data..
        if (actualZoom < min_zoom_for_filter) {
            return;
        }

        let polygonFeature;
        let extent = map.getView().calculateExtent(map.getSize());
        let polygon = new ol.geom.Polygon.fromExtent(extent);
        polygon.scale(0.5, 0.5)
        features = [new ol.Feature(polygon)];

        drawLayer.getSource().clear();
        drawSource.addFeatures(features);
        polygonFeature = new ol.Feature(new ol.geom.Polygon(features[0].getGeometry().getCoordinates()));

        let format = new ol.format.WKT();
        let src = 'EPSG:3857';
        let dest = 'EPSG:4326';
        let wktRepresenation = format.writeGeometry(polygonFeature.getGeometry().clone().transform(src,dest));

        let myFeatures;
        let plots = {};

        $.ajax({
            type: "GET",
            url: 'https://' + URL + '/index.php?query&qtable=pollimon_sample_plots&geom_selection=wktquery&geometry=' + wktRepresenation + '&output=json&filename=',
            dataType: 'json',
            //async: false,
            success: function (response) {
                let features = new Array;
                for (let k=0;k<response.length;k++) {
                    let feature = new ol.format.WKT().readFeatures(response[k].obm_geometry,{
                        'dataProjection': "EPSG:4326",
                        'featureProjection': "EPSG:3857"});
                    feature[0].setProperties(response[k]);
                    //plots[response[k].name] = response[k].observer;
                    plots["k" + response[k].obm_id] = {
                        'observer': response[k].observer,
                        'location_type': response[k].location_type,
                        'name': response[k].name,
                        'environment': response[k].environment,
                        'obm_files_id': response[k].obm_files_id
                      }
                    features.push(feature[0]);
                }
                
                // frissítjük az Alpine's store-t az új értékekkel
                Alpine.store('x').updateSites(plots);
                
                // Cluster
                clusterSource.getSource().clear(true);
                clusters.setVisible(true);
                clusterSource.getSource().addFeatures(features);
            }, 
            error: function () {
                Alpine.store('sites',{});
                setTab(0);
            }
        });
    };
