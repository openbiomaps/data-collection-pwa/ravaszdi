<?php
# An OpenBioMaps API client application
# @Miklós Bán
# 2023-01-01

require_once('settings.php.inc');
require_once('functions.php');
require_once('ajax.php');

$APP_PATH = basename(dirname(__FILE__));

// returning from Google Sign In
if (isset($_POST['credential'])) {
    //debugx('Google Call');
    define('PROJECT_DIR',basename(__DIR__));
    require_once '../includes/vendor/autoload.php';
    // Google redirect
    $client = new Google_Client(['client_id' => $_POST['client_id']]);  // a kliensazonosító beállítása
    $payload = $client->verifyIdToken($_POST['credential']);  // az ID Token ellenőrzése

    if ($payload) {
        $scopeRequired = "get_profile put_data";
        $client = CLIENT_ID;
        $client_secret = CLIENT_SECRET;
        // a felhasználó hitelesítve van, készítünk egy access_token-t neki
        require_once '../oauth/server.php';
        require_once '../oauth/google.php';

        //debugx($token,__FILE__,__LINE__);
        // beállítjuk a süti-be a tokeneket
        if (isset($token['access_token'])) {
            setcookie("access_token", $token['access_token']);
            setcookie("refresh_token", $token['refresh_token']);
        } else {
            // Log In to OBM failed
        }
    } else {
        // Log In to Google failed
        // érvénytelen ID Token
        //echo json_encode(array("error"=>"Invalid ID Token"));
        //die;
    }
    unset($_POST['credential']);
}

?>
<!doctype html>
<html lang="en">
<head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CODELAB: Add link rel manifest -->
  <link rel="manifest" href="manifest.json?v1">
<!-- CODELAB: Add iOS meta tags and icons -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="OpenBioMaps">
  <link rel="apple-touch-icon" href="images/icons/Android/Icon-144.png">
  <link rel="icon" href="https://openbiomaps.org/img/favicon.ico" type="image/x-icon" />
  <!-- description -->
  <meta name="description" content="OpenBioMaps Form App">
  <!-- meta theme-color -->
  <meta name="theme-color" content="#aad2dd" />
  <link rel="stylesheet" href="https://unpkg.com/purecss@2.1.0/build/pure-min.css" integrity="sha384-yHIFVG6ClnONEA5yB5DJXfW2/KC173DIQrYoZMEtBvGzmf0PKiGyNEqe9N6BNDBH" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="styles/form-styles.css?rev=<?php echo revx('styles/form-styles.css'); ?>">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/fontawesome.min.css">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/solid.min.css">
  <link rel="stylesheet" type="text/css" href="styles/inline.css?rev=<?php echo revx('styles/inline.css'); ?>">

  <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
  <!-- Local stroge -->
  <script src="scripts/localforage.js"></script>
  <!-- Keep screen on -->
  <script src="scripts/NoSleep.min.js"></script>
  <!-- OpenLayers -->
  <script src="https://cdn.jsdelivr.net/npm/ol@v8.2.0/dist/ol.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ol@v8.2.0/ol.css">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <!-- Alpine JS -->
  <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
  <!-- Crypto JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
    
  <title><?php echo TITLE ?></title>
</head>

<!-- PWA WakeLock -->
<script src="scripts/main.js?rev=<?php echo revx('scripts/main.js'); ?>"></script>

<body>

<script>
// Alpine Init
document.addEventListener('alpine:init', () => {
    // Some dialog's default state
    Alpine.store('showInfo', false);
    Alpine.store('showGPSbox', false);

    // A store for form specific objects 
    Alpine.store('x', {
        selectedSite: 'k0',
        sites: {'k0':{}},
        updateSites(newSites) {
            this.sites = newSites;
            this.selectedSite = Object.keys(newSites)[0];
        }
    });
});
</script>

<!-- Project specific javascript functions -->
<script src="scripts/ravaszdi_app.js?rev=<?php echo revx('scripts/ravaszdi_app.js'); ?>"></script>

<div id="maindiv" x-data="{ openForm: true, new_sample_plot: false, selected_sample_plot: true }">

<div id="map" class="map"></div>

<?php
/* AlpineJS .app-div
 *
 * */
require_once('app.php');

?>


<!-- Info modal -->
<div id='info' x-show="$store.showInfo" class="infomodal">
    <span class="close" x-on:click="$store.showInfo = false">&times;</span>
</div>

<!-- GPS position dialog -->
<div id="GPSbox" class="box" x-show="$store.showGPSbox">
      position accuracy : <code id="accuracy"></code>&nbsp;&nbsp;
      altitude : <code id="altitude"></code>&nbsp;&nbsp;
      altitude accuracy : <code id="altitudeAccuracy"></code>&nbsp;&nbsp;
      heading : <code id="heading"></code>&nbsp;&nbsp;
      speed : <code id="speed"></code>
</div>

<!-- footer banner -->
<div class="header">

    <div id="Menu" class="Menu" x-show="openMenu">
        <ul>
            <li @click="openMenu = !openMenu, welcomeOpen = true">Welcome</li>
        </ul> 
    </div>
    
    <div class="header-button" @click="openMenu = !openMenu" style='color:lightskyblue'>
        <i :class="{'material-icons': true}" title="Menü" style="vertical-align:bottom">more_vert</i>
    </div> &nbsp;


    <div class="header-button" @click="openForm = !openForm" style='color:lightskyblue'>
        <i :class="{'material-icons': true}" :title="openForm ? 'Térkép' : 'Adatgyűjtő űrlap'" style="vertical-align:bottom" x-text="openForm ? 'map' : 'edit_note'"></i>
    </div> &nbsp;
    
    <button id="butInstall" aria-label="Install" title="Telepít" hidden></button>

    <i class='fa-solid fa-eye' id="togglekeep" title="Wake Lock is enabled" style='color:lightskyblue;position:absolute;padding:0.4rem;right:0'></i>
</div><!-- main div -->

<script src="scripts/functions.js?rev=<?php echo revx('scripts/functions.js'); ?>"></script>
<script type="text/javascript">
    /* Cluster layer 
     * This is used when we are fetching spatial data  from the server and display on map
     * */
    const distanceInput = <?php echo $distanceInput ?>;
    const minDistanceInput = <?php echo $minDistanceInput ?>;
    const min_zoom_for_filter = <?php echo $min_zoom_for_filter ?>;
    const target_table = '<?php echo isset($_GET['table']) ? $_GET['table'] : PROJECTTABLE ?>';
    const wms_cluster = '<?php echo $wms_cluster ?>';
</script>
<script src="scripts/map_init.js?rev=<?php echo revx('scripts/map_init.js'); ?>"></script>
<script src="scripts/map_wms_fetch.js?rev=<?php echo revx('scripts/map_wms_fetch.js'); ?>"></script>
<script src="scripts/map_load.js?rev=<?php echo revx('scripts/map_load.js'); ?>"></script>

<script>
    // <!-- Register service worker. --->
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('service-worker.js')
            .then((reg) => {
              console.log('Service worker registered.', reg);
            });
      });
    }
</script>


<script src="scripts/install.js"></script>
</body>
</html>
