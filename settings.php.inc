<?php
# Developer setting
header('Access-Control-Allow-Origin: http://localhost');

# Page title
define('TITLE','Ravaszdi Radar');

# Weather
# GET API key from openweathermap
define('USE_WEATHER', false);
define('WEATHER_API_KEY','');

# OAuth
define('CLIENT_ID','');
define('CLIENT_SECRET','');

# WMS cluster
# layer name on OpenBioMaps: layer_data_MY-CLUSTER-LAYER for cname created automatically
#
# OpenBioMaps settings
define('PROJECTTABLE','ravaszdi');
define('URL','openbiomaps.org/projects/ravaszdi');
define('API_VERSION','2.4');
define('APP_PATH','pwa2');
# Cluster settings
$wms_cluster = 'my_cluster';
$min_zoom_for_filter = 12;
$distanceInput = 30;
$minDistanceInput = 1;

?>